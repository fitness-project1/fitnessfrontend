import type Member from "./Member";
import type Trainer from "./Trainer";

export default interface Account {
  account_id: number;
  account_phone: string;
  account_password: string;
  account_role: string;
  del: Date;
  member: Member[];
  trainer: Trainer[];
}