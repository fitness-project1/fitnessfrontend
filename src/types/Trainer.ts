export default interface Trainer {
  trainer_id: number;
  trainer_name: string;
  trainer_gender: string;
  trainer_dob: string;
  trainer_age: number;
  trainer_phone: string;
  trainer_email: string;
  trainer_address: string;
  trainer_facebook: string;
  trainer_start_date: string;
  trainer_style: string;
  del: Date;
  image: string;
}