import type Trainer from "./Trainer";

export default interface Class {
  classes_id?: number;
  classes_name?: string;
  classes_time?: string;
  classes_room?: number;
  classes_activity?: string;
  classes_qty?: number;
  classes_trainer?: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
  trainer?: Trainer;
  image?: string;

}