import type Member from "./Member";


export default interface Pt {
  pt_id?: number;
  pt_status?: string;
  pt_name?: string;
  pt_duration?: number;
  pt_trainer?: string;
  pt_createdAt?: Date;  
  delDate?: Date;
  member?: Member;
  image: string;
}