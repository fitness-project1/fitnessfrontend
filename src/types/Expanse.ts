import type Class from "./Class";
import type Member from "./Member";
import type Account from "./Account";

export default interface Expanse {
  expanse_id: number;
  expanse_type: string;
  expanse_name: string;
  expanse_date: Date;
  expanse_price: number;
  account : Account;
}