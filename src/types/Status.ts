import type Class from "./Class";
import type Member from "./Member";
import type Trainer from "./Trainer";

export default interface Status {
  status_id: number;
  status_name: string;
  status_booking_time: Date;
  member: Member;
  classes: Class;
}