

export default interface Member {
  member_id: number;
  member_name: string;
  member_gender: string;
  member_dob: Date;
  member_age: number;
  member_phone: string;
  member_email: string;
  member_address: string;
  member_start_date: Date;

}