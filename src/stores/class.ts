import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type Class from '@/types/Class';
import http from "@/services/axios";
import classService from "@/services/class"
import { useMessageStore } from './message';
import { useSuccessStore } from './success';

export const useClassStore = defineStore('Class', () => {
  const classes = ref<Class[]>([]);
  const editedClasses = ref<Class>({});

  
  async function getClasses() {
    try {
      const res = await classService.getClasses();
      classes.value = res.data;
      console.log(res)
    } catch (e) {
      console.log(e);
    }
  }
  async function deleteClass(classes_id: number) {
    try {
      const res = await classService.delClasses(classes_id);
      await getClasses();
    } catch(e) {
      console.log(e);
    }
  }

  // async function saveClass() {
  //   try {
  //     const res = await classService.saveClasses(editedClasses.value)
  //     await getClasses();
  //   } catch(e) {
  //     console.log(e);
  //   }
  // }
  async function saveClass() {
    try {
      const res = await classService.saveClasses(editedClasses.value)
      await getClasses();
    } catch(e) {
      console.log(e);
    }
  }
  

  
    return { classes, getClasses, deleteClass, editedClasses, saveClass  };
  });
