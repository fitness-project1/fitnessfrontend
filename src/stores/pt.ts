import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import type Pt from '@/types/Pt';
import ptService from "@/services/pt"
import type Member from '@/types/Member';

export const usePtStore = defineStore('pt', () => {
  const dialog = ref(false);
  const pt = ref<Pt[]>([]);
  const editedPt = ref<Pt>({pt_duration: 0, pt_trainer: "",});

  watch(dialog, (newDialog, oldDialog) => {
    if (!newDialog) {
      editedPt.value= { pt_duration: 0, pt_trainer: "",};
    }
  });
  async function getPts() {
    try {
      const res = await ptService.getPts();
      pt.value = res.data;
      console.log(res)
    } catch (e) {
      console.log(e);
    }

  }
  async function savePt() {
    try {
      const res = await ptService.savePts(editedPt.value)
      dialog.value = false;
      await getPts();
    } catch(e) {
      console.log(e);
    }
  }

  return { dialog, getPts, pt, savePt ,editedPt}
})