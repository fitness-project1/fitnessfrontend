import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type Class from '@/types/Class';
import http from "@/services/axios";
import statusService from "@/services/status"
import type Trainer from '@/types/Trainer';
import type Status from '@/types/Status';
import { useClassStore } from './class';

export const useStatusStore = defineStore('Status', () => {
  const status = ref<Status[]>([]);
  const classStore = useClassStore();
  async function getStatus() {
    try {
      const res = await statusService.getStatus();
      status.value = res.data;
      console.log(res)
    } catch (e) {
      console.log(e);
    }

  }
  
 async function saveStatus(classes_id: number) {
  try {
    const res = await statusService.saveStatus(classes_id);
    status.value = res.data;
    console.log(res)
    location.reload
  } catch (e) {
    console.log(e);
  }
}
    return { status, getStatus, saveStatus };
  });
