import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type Class from '@/types/Class';
import http from "@/services/axios";
import accountService from "@/services/account"
import type Trainer from '@/types/Trainer';
import trainer from '@/services/trainer';
import type Account from '@/types/Account';
import router from '@/router';
import { useMessageStore } from './message';

export const useAccountStore = defineStore('Account', () => {
  const account = ref<Account[]>([]);
  const messageStore = useMessageStore();
  async function getAccounts() {
    try {
      const res = await accountService.getAccounts();
      account.value = res.data;
      console.log(res)
    } catch (e) {
      console.log(e);
    }
  }
  async function login(accountPhone: string, accountPassword: string) {
    try {
      const accountFound = account.value.find(
        (acc) => acc.account_phone === accountPhone && acc.account_password === accountPassword
      );
      if (accountFound) {
        if (accountFound.account_role === 'Member') {
          router.push('/homeview');
        } else if (accountFound.account_role === 'Trainer') {
          router.push('/hometrainerview');
        } else if (accountFound.account_role === 'Admin') {
          router.push('/homeadminview');
        } else {
          console.log('Invalid account role');
        }
      } else {
        console.log('Account not found');
      }
    } catch (e) {
      console.log(e);
    }
  }
  const currentAccount = computed(() => {
    // ดึงข้อมูลของผู้ใช้ปัจจุบันที่ลงชื่อเข้าใช้
    return account.value.find(acc => acc.account_phone === localStorage.getItem('account_phone'));
  });
  
    return { account, getAccounts ,login, currentAccount};
  });
