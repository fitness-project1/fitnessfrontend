import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useSuccessStore = defineStore("success", () => {
  const dialog = ref(false);
  const success = ref("");

  function showsuccess(text: string) {
    success.value = text;
    dialog.value = true;
  }
  return { dialog, success, showsuccess };
});
