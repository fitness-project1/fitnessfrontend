import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type Class from '@/types/Class';
import http from "@/services/axios";
import expanseService from "@/services/expanse"
import type Expanse from '@/types/Expanse';

export const useExpanseStore = defineStore('Expanse', () => {
  const expanse = ref<Expanse[]>([]);
  async function getExp() {
    try {
      const res = await expanseService.getExpanses()
      expanse.value = res.data;
      console.log(res)
    } catch (e) {
      console.log(e);
    }

  }
  
    return { expanse, getExp };
  });
