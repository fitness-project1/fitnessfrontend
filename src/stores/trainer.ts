import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type Class from '@/types/Class';
import http from "@/services/axios";
import trainerService from "@/services/trainer"
import type Trainer from '@/types/Trainer';

export const useTrainerStore = defineStore('Trainer', () => {
  const trainer = ref<Trainer[]>([]);
  async function getTrainers() {
    try {
      const res = await trainerService.getTrainers();
      trainer.value = res.data;
      console.log(res)
    } catch (e) {
      console.log(e);
    }

  }
  
    return { trainer, getTrainers };
  });
