import type Class from '@/types/Class'
import http from './axios'

function getClasses() {
  return http.get('/classes')
}

// function saveClasses(classs: Class) {
//   return http.post('/classes', classs)
// }

function saveClasses(cs: Class) {
  return http.post('/classes', cs)
}

function delClasses(id: number) {
  return http.delete(`/classes/${id}`)
}

export default { getClasses, delClasses, saveClasses  }
