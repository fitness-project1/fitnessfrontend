import http from "./axios";

function getStatus(){
    return http.get("/status");
}
function saveStatus(classes: number) {
    return http.post("/status", { classes });
  }
 export default { getStatus, saveStatus};
