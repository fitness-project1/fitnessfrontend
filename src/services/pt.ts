import type Pt from '@/types/Pt'
import http from './axios'

function getPts() {
  return http.get('/pts')
}

function savePts(pt: Pt) {
  return http.post('/pts', pt)
}

export default { getPts, savePts }
