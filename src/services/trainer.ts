import http from "./axios";

function getTrainers(){
    return http.get("/trainers");
}

 
 export default { getTrainers };