import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import HomeVisitView from '../views/HomeVisitView.vue'
import ClassBooking from '@/components/members/ClassBooking.vue';
import PersonalTraining from '@/components/members/PersonalTraining.vue';
import ClassView from '@/components/members/ClassView.vue';
import ProfileManage from '@/components/members/ProfileManage.vue';
import StatusManage from '@/components/members/StatusManage.vue';
import MembershipView from '@/views/MembershipView.vue'
import PtManage from '@/components/members/trainer/PtManage.vue';
import StatusManageTrainer from '@/components/members/trainer/StatusManageTrainer.vue';
import StatusPtManageTrainer from '@/components/members/trainer/StatusPtManageTrainer.vue';
import HomeViewTrainer from '@/components/members/trainer/HomeViewTrainer.vue';
import ClassManage from '@/components/members/trainer/ClassManage.vue';
import ClassAddView from '@/components/members/trainer/ClassAddView.vue';
import StatusPtManage from '@/components/members/StatusPtManage.vue';
import LoginView from '@/views/LoginView.vue';
import RegisView from '@/views/RegisView.vue';
import HomeViewAdmin from '@/components/members/admin/HomeViewAdmin.vue';
import StatusManageAd from '@/components/members/admin/StatusManageAd.vue';
import StatusPtManageAd from '@/components/members/admin/StatusPtManageAd.vue';
import MoneyManage from '@/components/members/admin/MoneyManage.vue';
import TrainerManage from '@/components/members/admin/TrainerManage.vue';
import AddTrainer from '@/components/members/admin/AddTrainer.vue';
import ViewTrainer from '@/components/members/admin/ViewTrainer.vue';
import MembershipVisitView from '@/views/membership/MembershipVisitView.vue';
import ClassBookingVisit from '@/views/membership/ClassBookingVisit.vue';
import MembershipTrainerView from '@/components/members/trainer/MembershipTrainerView.vue';
import MembershipAdmin from '@/components/members/admin/MembershipAdmin.vue';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      redirect: '/homevisitview', // Redirect to /homeview by default
    },
    {
      path: '/homeview',
      name: 'homeview',
      component: HomeView
    },
    {
      path: '/homevisitview',
      name: 'homevisitview',
      component: HomeVisitView 
    },
    {
      path: '/loginview',
      name: 'loginview',
      component: LoginView
    },
    {
      path: '/regisview',
      name: 'regisview',
      component: RegisView
    },
    {
      path: '/classbooking',
      name: 'classbooking',
      component: ClassBooking
    },
    {
      path: '/ptbooking',
      name: 'ptbooking',
      component: PersonalTraining
    },
    {
      path: '/classview/:id', // เปลี่ยนจาก '/classview/' เป็น '/classview/:id'
      name: 'ClassView',
      component: ClassView,
      props: true
    },
    {
      path: '/profilemanage',
      name: 'profilemanage',
      component: ProfileManage
    },
    {
      path: '/statusmanage',
      name: 'statusmanage',
      component: StatusManage
    },
    {
      path: '/statusptmanage',
      name: 'statusptmanage',
      component: StatusPtManage
    },
    {
      path: '/statusmanaget',
      name: 'statusmanaget',
      component: StatusManageTrainer
    },
    {
      path: '/statusptmanaget',
      name: 'statusptmanaget',
      component: StatusPtManageTrainer
    },
    {
      path: '/statusmanagead',
      name: 'statusmanagead',
      component: StatusManageAd
    },
    {
      path: '/statusptmanagead',
      name: 'statusptmanagead',
      component: StatusPtManageAd
    },
    {
      path: '/membership',
      name: 'membership',
      component: MembershipView
    },
    {
      path: '/membershipvisit',
      name: 'membershipvisit',
      component: MembershipVisitView
    },
    {
      path: '/membershiptrainer',
      name: 'membershiptrainer',
      component: MembershipTrainerView
    },
    {
      path: '/membershipadmin',
      name: 'membershipadmin',
      component: MembershipAdmin
    },
    {
      path: '/hometrainerview',
      name: '/hometrainerview',
      component: HomeViewTrainer
    },
    {
      path: '/ptmanage',
      name: 'ptmanage',
      component: PtManage
    },
    {
      path: '/classmanage',
      name: 'classmanage',
      component: ClassManage
    },
    {
      path: '/classadd',
      name: 'classadd',
      component: ClassAddView
    },
    {
      path: '/homeadminview',
      name: '/homeadminview',
      component: HomeViewAdmin
    },
    {
      path: '/moneymanage',
      name: '/moneymanage',
      component: MoneyManage
    },
    {
      path: '/trainermanage',
      name: '/trainermanage',
      component: TrainerManage
    },
    {
      path: '/addtrainer',
      name: '/addtrainer',
      component: AddTrainer
    },
    {
      path: '/viewtrainer/:id',
      name: 'Viewtrainer',
      component: ViewTrainer,
      props: true
    },
    {
      path: '/classbookingvisit',
      name: 'classbookingvisit',
      component: ClassBookingVisit
    },
  ]
});



export default router;
